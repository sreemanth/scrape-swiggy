import React from "react";
import axios from "axios";
import { Table, Button, Container, Row, Col, Tab } from "react-bootstrap"

const SWIGGY_URL = `https://www.swiggy.com/dapi/restaurants/list/v5?lat=12.9715987&lng=77.5945627&sortBy=RELEVANCE&pageType=SEE_ALL&page_type=DESKTOP_SEE_ALL_LISTING`;
const SWIGGY_MENU_URL = `https://www.swiggy.com/dapi/menu/v4/full?lat=12.9715987&lng=77.5945627`;
class Swiggy extends React.Component {



    constructor(props) {
        super(props);
        this.state = { offset: 0, result: { restaurants: [] }, products: [] };
    }

    componentDidMount() {
        // this.load();
    }


    componentWillUnmount() {
    }

    nextStore = async (e) => {
        this.setState((state) => ({ offset: (state.offset + 15) }));
        console.log(this.state.offset);
        this.load();
    }

    prevStore = async (e) => {

        this.setState((state) => ({ offset: (state.offset - 15) > 0 ? (state.offset - 15) : 0 }));
        console.log(this.state.offset);
        this.load();
    }

    load() {
        axios.get(SWIGGY_URL + `&offset=${this.state.offset}`).then((resp) => {
            //setting result
            let resultJson = resp
            console.log(resultJson);
            let restaurants = [];
            for (let card of resultJson.data.data.cards) {
                if (card.cardType === "restaurant") {
                    restaurants.push(card.data.data)
                }
            }
            console.log(restaurants)
            this.setState({
                result: { restaurants: restaurants }
            })
        }).catch((error) => {
            console.log(error);
        })

    }

    loadProducts(selectedStoreId) {
        axios.get(SWIGGY_MENU_URL + `&menuId=${selectedStoreId}`).then((resp) => {
            //setting result
            let resultJson = resp
            let products = [];

            for (let [key, value] of Object.entries(resultJson.data.data.menu.items)) {
                products.push(value);
            }
            this.setState({
                products: products
            })
        }).catch((error) => {
            console.log(error);
        })

    }

    rowSelected = (restaurant, e) => {
        console.log(e);
        console.log(restaurant.id);
        this.setState({
            selectedStoreId: restaurant.id
        })
        this.loadProducts(restaurant.id);
    }

    render() {

        return (<div>
            <Container>
                <Row>
                    <Col><Button onClick={this.prevStore}>Previous</Button></Col>
                    <Col><Button onClick={this.nextStore}>Next</Button></Col>
                </Row>
            </Container>
            <Container>
                <Row>
                    <Col>
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                </tr>
                            </thead>

                            <tbody>{this.renderRestuarntTable()}</tbody></Table>
                    </Col>
                    <Col>
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Is Veg</th>
                                </tr>
                            </thead>
                            <tbody>{this.renderRestuarntProductsTable()}</tbody>
                        </Table>
                    </Col>
                </Row>
            </Container>

        </div>)
    }

    renderRestuarntTable() {
        return this.state.result.restaurants.map((restaurant, index) => {
            const { id, name, address } = restaurant;
            return (
                <tr key={id} onClick={this.rowSelected.bind(this, restaurant)}>
                    <td>{id}</td>
                    <td>{name}</td>
                    <td>{address}</td>
                </tr>
            )
        })
    }

    renderRestuarntProductsTable() {
        return this.state.products.map((product, index) => {
            const { id, name, price, isVeg } = product;
            return (
                <tr key={id} >
                    <td>{id}</td>
                    <td>{name}</td>
                    <td>{price/100}&#8377;</td>
                    <td>{isVeg}</td>
                </tr>
            )
        })
    }
}



export default Swiggy