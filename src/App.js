import React from 'react';
import logo from './logo.svg';
import './App.css';

import Swiggy from './Swiggy'
import Zomato from './Zomato';

import { Container, Row, Col, Navbar, Nav, Form, FormControl, Button } from "react-bootstrap";


function App() {
  return (
    <div className="App">
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand href="#home">Retail Data Scrapper</Navbar.Brand>
        <Nav className="mr-auto">
          <Nav.Link href="#home">Home</Nav.Link>
        </Nav>
        <Form inline>
          <FormControl type="text" placeholder="Search" className="mr-sm-2" />
          <Button variant="outline-light">Search</Button>
        </Form>
      </Navbar>
      <Container>
        <Row>
          <Col>
            <Swiggy></Swiggy>
          </Col>
        </Row>
        <Row>
          <Col>
            <p></p>
          </Col>
        </Row>

      </Container>

    </div>
  );
}

export default App;
